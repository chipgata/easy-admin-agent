#!/usr/bin/env python3
import json
import ssl
import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import os
import logging
import argparse
import easyAdminUtils
import commands
import jobs
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import time


class HomeDirChangeHandler(FileSystemEventHandler):
    def __init__(self, ws):
        self.ws = ws

    def on_any_event(self, event):
        what = 'directory' if event.is_directory else 'file'
        event_type = event.event_type
        print('%s %s: %s' % (what, event_type, event.src_path))
        logging.info('%s %s: %s' % (what, event_type, event.src_path))
        if what == 'file':
            if 'wp-content' in event.src_path:
                print("Ignore this path: %s" % event.src_path)
                logging.info("Ignore this path: %s" % event.src_path)
            else:
                if event_type == 'created' or  event_type == 'modified':
                    file_name = os.path.basename(event.src_path)
                    if file_name == '.htaccess':
                        send_msg(self.ws, commands.service_restart('olsws'))


def on_message(ws, message):
    print(message)
    logging.info('Msg received: %s' % message)
    jobs.init(ws, message)


def on_error(ws, error):
    print(error)
    logging.info(error)


def on_close(ws):
    print("### closed ###")
    logging.info('connection closed')


def on_open(ws):
    def run(*args):
        send_msg(ws, commands.register())
        print('connection established')
        logging.info('connection established')

    def monitor(ws):
        event_handler = HomeDirChangeHandler(ws)
        observer = Observer()
        observer.schedule(event_handler, '/home', recursive=True)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()

    thread.start_new_thread(run, ())
    thread.start_new_thread(monitor, (ws, ))


def send_msg(ws, msg):
    print('Msg sent: %s' % msg)
    logging.info('Msg sent: %s' % msg)
    ws.send(json.dumps(msg))


def logging_init(logf):
    script_name = os.path.splitext(os.path.basename(__file__))[0];
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename=logf, level=logging.INFO)
    logging.getLogger(script_name)


if __name__ == "__main__":
    easyAdminUtils.make_dir(os.getcwd() + '/logs')

    parser = argparse.ArgumentParser(description="An agent from centerpanel.io")
    parser.add_argument('-l', '--log-file', default=os.getcwd() + '/logs/debug.log')
    parser.add_argument('-s', '--ws-host', default='127.0.0.1')
    parser.add_argument('-p', '--ws-port', default='443')
    parser.add_argument('-r', '--room', default=easyAdminUtils.get_ip())
    #parser.add_argument('-u', '--user', default='test')
    #parser.add_argument('-m', '--password', default='aA*123456')
    parser.add_argument('-t', '--token', default='KLf-CwaeTCSvHhfOuT9YdIMCPx-X4Z7J8D8Y9QW5Bes')
    args = parser.parse_args()

    logging_init(logf=args.log_file)

    websocket.enableTrace(True)
    sslopt = {"cert_reqs": ssl.CERT_NONE}
    ws_url = "wss://%s:%s/ws/commander/%s/" % (args.ws_host, args.ws_port, args.room)
    ws_headers = {'token': args.token}
    ws = websocket.WebSocketApp(ws_url, header= ws_headers,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever(sslopt=sslopt)