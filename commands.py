import easyAdminUtils
import logging


def general():
    """general fields - options"""
    return {'commander': easyAdminUtils.get_sys_info()['node'], 'ip_address': easyAdminUtils.get_ip()}


def register():
    """register command first time connect to server"""
    return easyAdminUtils.merge_two_dicts(
                   general(),
                   {
                    'command': 'REGISTER',
                    'message': 'System info of client',
                    'data': easyAdminUtils.get_sys_info()
                    }
               )


def added_public_key():
    """register command first time connect to server"""
    return easyAdminUtils.merge_two_dicts(
                    general(),
                    {
                        'command': 'ADDED_PUBLIC_KEY',
                        'message': 'Client added ssh public key already',
                        'data': {}
                    }
                )


def service_restart(service_name):
    """server restart command for some reason"""
    return easyAdminUtils.merge_two_dicts(
                    general(),
                    {
                        'command': 'RESTART_SERVICE',
                        'message': 'Please restart this service',
                        'data': {'service_name': service_name}
                    }
                )

