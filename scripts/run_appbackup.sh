#!/bin/sh

BACKUP_DIR=/opt/centerpanel/backup/app/
SOURCE_PATH=$1
SECRET_KEY=$2
FILE_PREFIX=$3
LOCAL_FULL_EVERY=$4
LOCAL_KEEP_FULL=$5
LOCAL_KEEP_INCR=$6

export PASSPHRASE=$SECRET_KEY; duplicity --full-if-older-than $LOCAL_FULL_EVERY --file-prefix $FILE_PREFIX- $SOURCE_PATH file://$BACKUP_DIR
export PASSPHRASE=$SECRET_KEY; duplicity remove-all-but-n-full $LOCAL_KEEP_FULL --force file://$BACKUP_DIR
export PASSPHRASE=$SECRET_KEY; duplicity remove-all-inc-of-but-n-full $LOCAL_KEEP_INCR --force file://$BACKUP_DIR