from pydrive2.auth import GoogleAuth
from pydrive2.drive import GoogleDrive
from pydrive2.files import GoogleDriveFileList
import googleapiclient.errors
import os
import logging
import argparse
import time
import subprocess
import ast
import requests
import ntpath
import glob
import datetime


def logging_init(logf):
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename=logf, level=logging.INFO)
    logging.getLogger(script_name)


def make_dir(directory):
    """check if dir exist ignor, unless create"""
    if not os.path.exists(directory):
        os.makedirs(directory)


def is_tool(name):
    p = subprocess.Popen(['/usr/bin/which', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.communicate()
    return p.returncode == 0


def gg_get_folder_id(drive, parent_folder_id, folder_name):
    """
		Check if destination folder exists and return it's ID
	"""

    # Auto-iterate through all files in the parent folder.
    file_list = GoogleDriveFileList()
    try:
        file_list = drive.ListFile({'q': "'{0}' in parents and trashed=false".format(parent_folder_id)}).GetList()
    # Exit if the parent folder doesn't exist
    except googleapiclient.errors.HttpError as err:
        # Parse error message
        message = ast.literal_eval(err.content)['error']['message']
        if message == 'File not found: ':
            print(message + folder_name)
            logging.error('File not found: ' + message + folder_name)
            exit(1)
        # Exit with stacktrace in case of other error
        else:
            raise

    # Find the the destination folder in the parent folder's files
    for file in file_list:
        if file['title'] == folder_name:
            print('get meta data success: title: %s, id: %s' % (file['title'], file['id']))
            logging.info('get meta data success: title: %s, id: %s' % (file['title'], file['id']))
            return file['id']


def gg_create_folder(drive, folder_name, parent_folder_id):
    """
		Create folder on Google Drive
	"""

    folder_metadata = {
        'title': folder_name,
        # Define the file type as folder
        'mimeType': 'application/vnd.google-apps.folder',
        # ID of the parent folder
        'parents': [{"kind": "drive#fileLink", "id": parent_folder_id}]
    }


    folder = drive.CreateFile(folder_metadata)
    folder.Upload()

    # Return folder informations
    print('created folder: title: %s, id: %s' % (folder['title'], folder['id']))
    logging.info('created folder: title: %s, id: %s' % (folder['title'], folder['id']))
    return folder['id']


def gg_drive_upload(source_path, prefix, type):
    gauth = GoogleAuth(settings_file=os.path.dirname(os.path.abspath(__file__)) + '/settings.yaml')
    gauth.LoadCredentialsFile(os.path.dirname(os.path.abspath(__file__)) + "/credentials.json")

    if gauth.credentials is None:
        # Authenticate if they're not there
        print("You have not authenticated yet. Please back to dashboard and reactive again.")
        logging.info("You have not authenticated yet. Please back to dashboard and reactive again.")
        return False
    elif gauth.access_token_expired:
        # Refresh them if expired
        print("Token Expired, refesh token is doing.")
        logging.info("Token Expired, refesh token is doing.")
        gauth.Refresh()
    else:
        # Initialize the saved creds
        gauth.Authorize()

    # Save the current credentials to a file
    gauth.SaveCredentialsFile(os.path.dirname(os.path.abspath(__file__)) + "/credentials.json")

    drive = GoogleDrive(gauth)

    prefix_folder_id = gg_get_folder_id(drive, 'root', prefix)
    if not prefix_folder_id:
        print('creating prefix folder: ' + prefix)
        logging.info('creating prefix folder: ' + prefix)
        prefix_folder_id = gg_create_folder(drive, prefix, 'root')
    else:
        print('prefix folder {0} already exists'.format(prefix))
        logging.info('prefix folder {0} already exists'.format(prefix))

    folder_id = gg_get_folder_id(drive, prefix_folder_id, type)
    if not folder_id:
        print('creating folder: ' + prefix + '/' + type)
        logging.info('creating folder: ' + prefix + '/' + type)
        folder_id = gg_create_folder(drive, type, prefix_folder_id)
    else:
        print('folder {0} already exists'.format(prefix + '/' + type))
        logging.info('folder {0} already exists'.format(prefix + '/' + type))

    file = drive.CreateFile(
        {"parents": [{"kind": "drive#fileLink", "id": folder_id}], "title": os.path.basename(source_path)})
    file.SetContentFile(source_path)
    file.Upload()

    if file:
        print('Uploaded success object title: %s, id: %s' % (file['title'], file['id']))
        logging.info('Uploaded success object title: %s, id: %s' % (file['title'], file['id']))
        return True


def status_update(api, data, headers):
    try:
        print('Calling %s to update status of backup.' % api, data)
        logging.info('Calling %s to update status of backup.' % api, data)
        center_panel_api_url = api
        r = requests.post(center_panel_api_url, data=data, headers=headers)
        print('Response of status update: ' + r.text)
        logging.info('Response of status update: ' + r.text)
        if r.status_code == 201:
            return True
        return False
    except requests.exceptions.Timeout:
        print('Connection %s timeout.' % api)
        logging.info('Connection %s timeout.' % api)
    except requests.exceptions.RequestException as e:
        print('Exception happening during request to this API %s.' % api)
        logging.info('Exception happening during request to this API %s.' % api)
        raise SystemExit(e)


def db_process(db, user, password, secret_key):
    print('Database backup is starting....')
    logging.info('Database backup is starting....')

    if not is_tool('mariabackup'):
        print('mariabackup command not found.')
        logging.error('mariabackup command not found.')
        return False

    if not is_tool('gzip'):
        print('gzip command not found.')
        logging.error('gzip command not found.')
        return False

    datetime = time.strftime('%Y%m%d_%H%M%S')
    #temp_path = '/tmp/' + db + '_' + datetime + '.sql.gz'
    #cmd = 'mysqldump -u ' + user + ' -p' + password + ' ' + db + ' | gzip > ' + temp_path
    cmd = 'sh /opt/centerpanel/agent/scripts/run_mariabackup.sh ' + user + ' ' + password + ' ' + db + ' ' + secret_key
    print('Comand is doing: ' + cmd)
    logging.info('Comand is doing: ' + cmd)

    try:
        p = subprocess.Popen(cmd, shell=True, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
    except subprocess.CalledProcessError:
        print('Something wrong on db backup command: ' + cmd)
        logging.error('Something wrong on db backup command: ' + cmd)
        return False

    backup_db_dir = os.path.dirname(os.path.abspath(__file__)) + '/../../' + '/backup/db/'
    last_completed_backup_path = backup_db_dir + db + '_last_completed_backup'
    if not os.path.isfile(last_completed_backup_path):
        print('last completed backup path not found')
        logging.error('last completed backup path not found')
        return False

    with open(last_completed_backup_path) as f:
        last_backup_file = f.readline()

    backup_size = os.path.getsize(last_backup_file)
    size = get_backup_size(backup_size)
    timestamp = time.strftime('%Y-%m-%dT%H:%M:%S')
    msg = 'Backup database: ' + db + ' finished at ' + str(timestamp) + '. Size: ' + size
    print(msg)
    logging.info(msg)
    return last_backup_file


def app_process(domain, owner, secret_key, local_full_every, local_keep_full, local_keep_incr):
    print('Application backup is starting....')
    logging.info('Application backup is starting....')

    if not is_tool('duplicity'):
        print('duplicity command not found.')
        logging.error('duplicity command not found.')
        return False

    web_path = '/home/' + owner + '/web/' + domain + '/'
    #datetime = time.strftime('%Y%m%d_%H%M%S')
    #temp_path = '/tmp/' + domain + '_' + datetime + '.tar.gz'
    temp_path = '/opt/centerpanel/backup/app/'
    cmd = 'export PASSPHRASE=' + secret_key + '; duplicity --full-if-older-than ' + local_full_every + \
                                                    ' --file-prefix '+ domain +'- ' + web_path + ' file://' + temp_path

    cmd2 = 'export PASSPHRASE=' + secret_key + '; duplicity remove-all-but-n-full ' + str(local_keep_full) + \
           ' --force file://' + temp_path

    cmd3 = 'export PASSPHRASE=' + secret_key + '; duplicity remove-all-inc-of-but-n-full ' + str(local_keep_incr) + \
           ' --force file://' + temp_path

    print('Comand_1 is doing: ' + cmd)
    logging.info('Comand_1 is doing: ' + cmd)
    print('Comand_2 is doing: ' + cmd2)
    logging.info('Comand_2 is doing: ' + cmd2)
    print('Comand_3 is doing: ' + cmd2)
    logging.info('Comand_3 is doing: ' + cmd2)

    try:
        p = subprocess.Popen(cmd, shell=True, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
    except subprocess.CalledProcessError:
        print('Something wrong on app backup command: ' + cmd)
        logging.error('Something wrong on app backup command: ' + cmd)
        return False

    try:
        p2 = subprocess.Popen(cmd2, shell=True, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p2.communicate()
    except subprocess.CalledProcessError:
        print('Something wrong on app backup command: ' + cmd2)
        logging.error('Something wrong on app backup command: ' + cmd2)
        return False

    try:
        p3 = subprocess.Popen(cmd3, shell=True, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p3.communicate()
    except subprocess.CalledProcessError:
        print('Something wrong on app backup command: ' + cmd3)
        logging.error('Something wrong on app backup command: ' + cmd3)
        return False

    return temp_path


def get_backup_size(size):
    value = 'KB'
    #size = os.path.getsize(file_path)
    size = size / 1024  # KB
    if size > 1024:
        size = size / 1024  # MB
        value = 'MB'
    return str(size) + value


def process(app_id, domain, owner, db_name, db_user, db_pass, prefix, token, api, secret_key, local_full_every, local_keep_full, local_keep_incr):
    db_source = db_process(db_name, db_user, db_pass, secret_key)
    if db_source:
        print('Uploading db backup to google drive....')
        gg_drive_upload(db_source, prefix, 'db')

    app_backup_start_time = time.strftime('%Y-%m-%dT%H:%M:%S')
    app_source = app_process(domain, owner, secret_key, local_full_every, local_keep_full, local_keep_incr)
    if app_source:
        print('Uploading app backup to google drive....')
        backup_files = sorted(glob.iglob(app_source + domain + "-*"), key=os.path.getctime, reverse=True)
        backup_size = 0

        for x in range(0, 2):
            r = gg_drive_upload(backup_files[x], prefix, 'app')
            backup_size += os.path.getsize(backup_files[x])

        size = get_backup_size(backup_size)
        app_backup_end_time = time.strftime('%Y-%m-%dT%H:%M:%S')
        msg = 'Backup app: ' + domain + ' finished at ' + str(app_backup_end_time) + '. Size: ' + size
        print(msg)
        logging.info(msg)

        if r:
            if not token:
                print("No token to update status. Please contact admin to get token.")
                logging.info("No token to update status. Please contact admin to get token.")
                return False

            headers = {
                'Authorization': "Token " + token,
            }
            data = {
                'app': app_id,
                'status': True,
                'start_time': app_backup_start_time,
                'end_time': app_backup_end_time,
                'src_path': '/home/' + owner + '/web/' + domain + '/',
                'desc_path': prefix + '/app/' + ntpath.basename(app_source),
                'size': size,
                'message': msg,
            }
            status_update(api, data, headers)


if __name__ == "__main__":
    make_dir(os.getcwd() + '/logs')

    parser = argparse.ArgumentParser(description="An backup scripting from centerpanel.io")
    parser.add_argument('-l', '--log-file', default=os.path.dirname(os.path.abspath(__file__)) + '/../' + '/logs/backup.log')
    parser.add_argument('-o', '--owner', required=True)
    parser.add_argument('-p', '--prefix', default='centerpanel_backup')
    parser.add_argument('-b', '--backup-driver', default='google_driver')
    parser.add_argument('-t', '--token', required=True)
    parser.add_argument('-a', '--api', default='http://127.0.0.1:8000/clients/api/v1/backup/app/')
    parser.add_argument('-n', '--domain', required=True)
    parser.add_argument('-i', '--app-id', required=True)
    parser.add_argument('-dn', '--db-name', required=True)
    parser.add_argument('-du', '--db-user', required=True)
    parser.add_argument('-dp', '--db-pass', required=True)
    parser.add_argument('-s', '--secret-key', required=True)
    parser.add_argument('-lfe', '--local-full-every', default='7D')
    parser.add_argument('-lkf', '--local-keep-full', default=2)
    parser.add_argument('-lki', '--local-keep-incr', default=2)
    args = parser.parse_args()

    logging_init(logf=args.log_file)

    process(
            args.app_id,
            args.domain,
            args.owner,
            args.db_name,
            args.db_user,
            args.db_pass,
            args.prefix,
            args.token,
            args.api,
            args.secret_key,
            args.local_full_every,
            args.local_keep_full,
            args.local_keep_incr
    )