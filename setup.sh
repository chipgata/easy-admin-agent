#!/bin/bash
## An scripting by centerpanel.io, which help to setup agent easier
## Author: chipgata
## Version: 0.0.1"
## Usage: ./setup.sh YOUR_TOKEN
## Supports:
##   Centos - 7, 8
##   Ubuntu - Dont support yet
##

SOURCE_PATH=/opt/centerpanel/agent
SUPERVISOR_CONF=/etc/supervisord.d/centerpanel-agent.ini
SERVER_SSH_PORT=22
function lowercase {
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

function getSSHPort {
  SSH_PORT=`netstat -nlpt|grep sshd|head -1|awk '{print $4}'|cut -d: -f2`
}

function getOs {
    OS=`lowercase \`uname\``
    KERNEL=`uname -r`
    MACH=`uname -m`

    if [ "{$OS}" == "windowsnt" ]; then
        OS=windows
    elif [ "{$OS}" == "darwin" ]; then
        OS=mac
    else
        OS=`uname`
        if [ "${OS}" = "SunOS" ] ; then
            OS=Solaris
            ARCH=`uname -p`
            OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
        elif [ "${OS}" = "AIX" ] ; then
            OSSTR="${OS} `oslevel` (`oslevel -r`)"
        elif [ "${OS}" = "Linux" ] ; then
            if [ -f /etc/redhat-release ] ; then
                DistroBasedOn='RedHat'
                DIST=`cat /etc/redhat-release |sed s/\ release.*//`
                PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
                REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
                VER=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// |cut -d. -f1`
            elif [ -f /etc/SuSE-release ] ; then
                DistroBasedOn='SuSe'
                PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
                REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
            elif [ -f /etc/mandrake-release ] ; then
                DistroBasedOn='Mandrake'
                PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
                REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
            elif [ -f /etc/debian_version ] ; then
                DistroBasedOn='Debian'
                DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
                PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
                REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
                VER=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }' |cut -d. -f1`
            fi
            if [ -f /etc/UnitedLinux-release ] ; then
                DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
            fi
            OS=`lowercase $OS`
            DistroBasedOn=`lowercase $DistroBasedOn`
            readonly OS
            readonly DIST
            readonly DistroBasedOn
            readonly PSUEDONAME
            readonly REV
            readonly KERNEL
            readonly MACH
            readonly VER
        fi

    fi
}

function cloneSource {
  git clone https://gitlab.com/chipgata/easy-admin-agent.git $SOURCE_PATH
}

function tokenChange {
    local search='YOUR_TOKEN'
    local replace=$1

    cp supervisord_$2.conf $SUPERVISOR_CONF
    sed -i "s/${search}/${replace}/g" $SUPERVISOR_CONF
}

function restartCentosSupervisord {
  if [[ $1 -gt 6 ]]
    then
        systemctl restart supervisord
        systemctl enable supervisord
  else
      service supervisord restart
      chkconfig supervisord on
  fi
}

function centosSetup {
  #Install EPEL repo and supervisor, git
  echo "//------------------------Install EPEL repo and supervisor, git--------------------------------//"
  sudo yum -y install epel-release
  sudo yum -y install supervisor git

  #Install pip
  echo "//------------------------Install python pip--------------------------------//"
  sudo yum -y install python-pip

  #Install some packages
  echo "//------------------------Install some packages--------------------------------//"
  sudo yum -y install python-devel librsync-devel gcc

  #get source and chang working dir
  echo "//------------------------get source and chang working dir--------------------------------//"
  cloneSource
  cd $SOURCE_PATH

  #update token to supervisor
  echo "//------------------------Update token to supervisor--------------------------------//"
  tokenChange $1 $2

  #Install python libraries
  echo "//------------------------Install python libraries--------------------------------//"
  sudo pip install --upgrade pip
  sudo pip install --upgrade setuptools
  sudo pip install -r requirements.txt

  echo "//------------------------Starting centerpanel agent--------------------------------//"
  restartCentosSupervisord $VER
}


TOKEN=$1
ENV="prod"

if [ -z "$TOKEN" ]
then
      echo "Please input your token, login into centerpanel.io by your account to get the token."
      exit 1
fi

if [ -n "$2" ]
then
      ENV=$2
fi

#Get OS, Distro and version info
getOs

echo "##An scripting by centerpanel.io, which help to setup agent easier"
echo "## Author: chipgata"
echo "## Version: 0.0.1"
echo "## Usage: ./setup.sh YOUR_TOKEN"
echo "## Supports:"
echo "##   Centos - 6, 7, 8"
echo "##   Ubuntu - Dont support yet"
echo "##"
echo "//------------------------Your Linux distro information--------------------------------//"
echo "OS: ${OS}"
echo "Distro based on: ${DistroBasedOn}"
echo "DIST: ${DIST}"
echo "VERSION: ${VER}"

case "$DIST" in
        "CentOS Linux")
            centosSetup $TOKEN $ENV
            ;;
        *)
            echo $"//------This script did not support current OS distro. Usage: $0 YOUR_TOKEN"
            exit 1
esac