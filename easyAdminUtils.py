import os
import platform
import logging
import netifaces
import distro
import subprocess
import sys


def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def logging_init(file_name):
    script_name = os.path.splitext(os.path.basename(__file__))[0];
    logf = os.getcwd() + '/logs/' + file_name
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename=logf, level=logging.INFO)
    logging.getLogger(script_name)


def get_sys_info():
    """get system info"""
    r = {}
    if sys.version_info >= (3, 0):
        r = platform.uname()._asdict()
    else:
        r['system'] = platform.uname()[0]
        r['node'] = platform.uname()[1]
        r['release'] = platform.uname()[2]
        r['version'] = platform.uname()[3]
        r['machine'] = platform.uname()[4]
        r['processor'] = platform.uname()[5]
    r['ssh_port'] = 22

    if get_distro_info():
        r['distro'] = get_distro_info()[0]
        r['distro_version'] = get_distro_info()[1]
        r['ssh_port'] = get_ssh_port(get_distro_info()[0])

    if(get_agent_version()):
        r['cp_agent_version'] = get_agent_version()

    return r


def get_distro_info():
    d = distro.linux_distribution()
    return d


def get_ip():
   """get network info"""
   try:
      gws = netifaces.gateways()
      gw_default = gws['default'][netifaces.AF_INET]

      if not gw_default:
          return None

      addrs = netifaces.ifaddresses(gw_default[1])
      if not addrs:
          return None

      ips =  addrs[netifaces.AF_INET]
      if not ips:
          return None

      return ips[0]['addr']
   except:
      print("Unable to get IP")


def make_dir(directory):
    """check if dir exist ignor, unless create"""
    if not os.path.exists(directory):
        os.makedirs(directory)


def get_user_home_dir():
    """get home directory of current loggin user"""
    return os.path.expanduser('~')


def check_file_exist(file):
    """check if a file exist or not"""
    if os.path.isfile(file):
        return True
    return False


def write_line_to_file(file_name, text_to_append):
    """Append given text as a new line at the end of file"""
    # Open the file in append & read mode ('a+')
    with open(file_name, "a+") as file_object:
        # Move read cursor to the start of file.
        file_object.seek(0)
        # If file is not empty then append '\n'
        data = file_object.read(100)
        if len(data) > 0:
            file_object.write("\n")
        # Append text at the end of file
        file_object.write(text_to_append)


def check_if_string_in_file(file_name, string_to_search):
    """ Check if any line in the file contains given string """
    if not check_file_exist(file_name):
        return False

    # Open the file in read only mode
    with open(file_name, 'r') as read_obj:
        # Read all lines in the file one by one
        for line in read_obj:
            # For each line, check if line contains the string
            if string_to_search in line:
                return True
    return False


def add_public_key(file='.ssh/authorized_keys', key_content=None):
    """add public key to server after connect"""
    ssh_path = get_user_home_dir() + '/' + '.ssh'
    make_dir(ssh_path)
    path = get_user_home_dir() + '/' + file
    if check_if_string_in_file(path, key_content):
        return
    write_line_to_file(path, key_content)


def get_ssh_port(distro=None):
    """get ssh port of server"""
    if not distro:
        print("No distro found")
        return 22

    if distro.lower() == 'centos linux':
        command = "netstat -nlpt|grep sshd|head -1|awk '{print $4}'|cut -d: -f2"
        process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        proc_stdout = int(process.communicate()[0].replace('\n', ''))
        return proc_stdout
    else:
        return 22


def get_agent_version(filename='./version.txt'):
    with open(filename) as f:
        return f.read()