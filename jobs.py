import easyAdminUtils
import logging
import json
import commands

def init(ws, msg = None):
    """INIT command first time connect to server"""
    try:
        if not msg:
            logging.info("jobs - No msg to process: %s" % msg)
            return

        data_json = json.loads(msg)
        logging.info("jobs - data json: %s" % data_json)

        if 'command' not in data_json:
            logging.info("jobs - command not found: %s" % data_json)
            return

        eval(data_json['command'].lower())(ws, data_json)
    except:
        logging.info("jobs - function not found: %s" % data_json)

def send_msg(ws, msg):
    print('Msg sent: %s' % msg)
    logging.info('Msg sent: %s' % msg)
    ws.send(json.dumps(msg))

def add_public_key(ws, msg=None):
    logging.info('Calling add_public_key function %s' % msg)
    public_key = msg['data']['public_key']
    easyAdminUtils.add_public_key(key_content=public_key)
    send_msg(ws, commands.added_public_key())
